import React from "react";
import { Link } from "react-router-dom";
import { Form, Button, Card } from "react-bootstrap";

export default function  RegisterForm(props) {
  return (
    <Card style={{ width: "31rem" }}>
      <div className="p-3 border-bottom align-self-center d-flex align-items-center justify-content-center">
        <h5>Registro a TODOAPP</h5>
      </div>
      <div className="p-3 px-4 py-4 border-bottom">
        {props.errorMessage}
        <Form onSubmit={props.handleRegister}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Nombres</Form.Label>
            <Form.Control
              type="text"
              placeholder="Jhon Doe"
              name="nombre"
              value={props.state.form.nombre}
              onChange={(e) => props.updateForm(e)}
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Apellidos</Form.Label>
            <Form.Control
              type="text"
              placeholder="Veliz"
              name="apellido"
              value={props.state.form.apellido}
              onChange={(e) => props.updateForm(e)}
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Correo Electrónico</Form.Label>
            <Form.Control
              type="correo"
              placeholder="info@mail.com"
              name="correo"
              value={props.state.form.correo}
              onChange={(e) => props.updateForm(e)}
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Clave</Form.Label>
            <Form.Control
              type="password"
              placeholder=""
              name="clave"
              value={props.state.form.clave}
              onChange={(e) => props.updateForm(e)}
            />
          </Form.Group>

          <Button variant="primary" type="submit">
            Registrarse
          </Button>
        </Form>
      </div>
      <div className="p-3 d-flex flex-row justify-content-center align-items-center member">
        ¿Ya tienes cuenta?
        <Link to="/" className="text-decoration-none ml-2">
          Ingresa
        </Link>
      </div>
    </Card>
  );
}
