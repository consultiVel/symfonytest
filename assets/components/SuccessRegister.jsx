import React from "react";
import { Link } from "react-router-dom";
import { Card, Alert } from "react-bootstrap";

export default function SuccessRegister(props) {
  return (
    <Card style={{ width: "31rem" }}>
      <div className="p-3 border-bottom align-self-center d-flex align-items-center justify-content-center">
        <h5>Registro a TODOAPP</h5>
      </div>
      <div className="p-3 px-4 py-4 border-bottom">
        <Alert variant={"success"}>
          {props.nombre}, Se ha registrado correctamente
        </Alert>
        <Link to="/" className="text-decoration-none ml-2">
          Ingresa
        </Link>
      </div>
    </Card>
  );
}
