import React, { Component } from "react";
import { Table, Container, Button, Badge } from "react-bootstrap";

import Http from "./../lib/Http";
import ShowTask from "./ShowTask";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      showModalHistory: false,
      tareaSelected: null,
      histories: [],
      showModalCreateEdit: false,
    };
  }

  loadTasks = async () => {
    const { id } = JSON.parse(localStorage.getItem("user"));
    const req = await Http.instance.get(`/api/tareas?id=${id}`);

    return req.data;
  };

  async loadHistory(tareaSelected) {
    const req = await Http.instance.get(
      `/api/tarea/${tareaSelected}/historial`
    );
    this.setState({ histories: req.data });
  }

  toggleModalHistory = (state, tareaSelected) => {
    this.setState({ showModalHistory: state });
    this.setState({ tareaSelected });
  };

  toggleModalCreateEditTask = (state, tareaSelected) => {
    this.setState({ showModalCreateEdit: state });
    this.setState({ tareaSelected });
  };

  setVariantColor(stateId) {
    let color = "";
    switch (stateId) {
      case 1:
        color = "primary";
        break;
      case 2:
        color = "success";
        break;
      case 3:
        color = "warning";
        break;
      case 4:
        color = "secondary";
        break;
      default:
        color = "primary";
        break;
    }

    return color;
  }

  async componentDidMount() {
    const tasks = await this.loadTasks();
    this.setState({ tasks });
  }

  render() {
    return (
      <>
        <ShowTask
          showModalHistory={this.state.showModalHistory}
          handleHide={this.toggleModalHistory}
          tareaSelected={this.state.tareaSelected}
          histories={this.state.histories}
        />

        <CreateEditTask
          showModalCreateEdit={this.state.showModalCreateEdit}
          handleHide={this.toggleModalCreateEditTask}
        />

        <Container>
          <Button onclick={this.createTask} variant="primary">
            Crear Tarea{" "}
          </Button>

          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Titulo</th>
                <th>Estado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              {this.state.tasks.map((task, idx) => (
                <tr key={idx}>
                  <td>{idx + 1}</td>
                  <td>{task.titulo}</td>
                  <td>
                    <Badge bg={this.setVariantColor(task.estado_id)}>
                      {task.nombre_estado}
                    </Badge>
                  </td>
                  <td>
                    <Button
                      variant="primary"
                      onClick={() => {
                        this.toggleModalHistory(true),
                          this.loadHistory(task.id);
                      }}
                    >
                      Ver
                    </Button>
                    {"  "}
                    {task.estado_id != "4" ? (
                      <Button
                        variant="primary"
                        onClick={() => {
                          this.toggleModalHistory(true),
                            this.loadHistory(task.id);
                        }}
                      >
                        Editar
                      </Button>
                    ) : (
                      ""
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      </>
    );
  }
}

export default Home;
