import { Modal, Table } from "react-bootstrap";
import React from "react";



export default class ShowTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      histories: props.histories,
    };
  }

  handleClose = () => this.props.handleHide(false);

  render() {
    return (
      <Modal show={this.props.showModalHistory} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Historial</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table>
            <thead>
              <tr>
                <th>#</th>
                <th>Estado</th>
                <th>Fecha de Creación</th>
              </tr>
            </thead>
            <tbody>
              {this.state.histories.map((history, idx) => {
                <tr>
                  <td>{history.idx + 1}</td>
                  <td>{history.estado_id}</td>
                  <td>{history.fechaCreacion}</td>
                </tr>;
              })}
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
    );
  }
}
