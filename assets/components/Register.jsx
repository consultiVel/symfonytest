import React, { Component } from "react";

import { Form, Button, Card, Alert } from "react-bootstrap";

import RegisterForm from "./RegisterForm";
import SuccessRegister from "./SuccessRegister";
import Http from "./../lib/Http";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        nombre: "",
        apellido: "",
        correo: "",
        clave: "",
      },
      formErrors: {
        nombre: "",
        apellido: "",
        correo: "",
        clave: "",
      },
      hasError: false,
      messageError: "",
      hasSuccess: false,
      messageSuccess: "",
    };
    this.updateForm = this.updateForm.bind(this);
  }

  handleRegister = async (e) => {
    this.setState({ hasError: false });
    this.setState({ hasSuccess: false });
    e.stopPropagation();
    e.preventDefault();
    let data = this.state.form;

    const req = await Http.instance.post("/api/register", data);

    if (typeof req === "object" && req.hasOwnProperty("data")) {
      this.setState({ hasSuccess: true });
      this.setState({ messageSuccess: req.data.nombre });
    } else {
      this.setState({ hasError: true });
      this.setState({ messageError: req.message });
      this.setMessageErrorsForm(req.errors.children);
    }
  };

 

  updateForm(e) {
    let form = this.state.form;
    const currentForm = form;
    const { name, value } = e.target;
    currentForm[name] = value;
    this.setState({ form: currentForm });
  }

  render() {
    let errorMessage = null;
    let domShowForUser = null;

    if (this.state.hasError) {
      errorMessage = (
        <Alert variant={"danger"}>{this.state.messageError}</Alert>
      );
    }

    if (!this.state.hasSuccess) {
      domShowForUser = (
        <RegisterForm
          errorMessage={errorMessage}
          handleRegister={this.handleRegister}
          updateForm={this.updateForm}
          state={this.state}
        />
      );
    } else {
      domShowForUser = <SuccessRegister nombre={this.state.messageSuccess} />;
    }
    return (
      <div className="container d-flex align-self-center   justify-content-center align-items-center">
        {domShowForUser}
      </div>
    );
  }
}

export default Register;
