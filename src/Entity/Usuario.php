<?php

namespace App\Entity;

use App\Repository\UsuarioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UsuarioRepository::class)
 */
class Usuario
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $correo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clave;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_creacion;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_actualizacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity=Tarea::class, mappedBy="Usuario")
     */
    private $tareas;

    public function __construct()
    {
        // we set up "created"+"modified"
        $this->setFechaCreacion(new \DateTime());
        $this->setEstado(1);
        if ($this->getFechaActualizacion() == null) {
            $this->setFechaActualizacion(new \DateTime());
        }
        $this->tareas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fecha_creacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fecha_creacion): self
    {
        $this->fecha_creacion = $fecha_creacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fecha_actualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fecha_actualizacion): self
    {
        $this->fecha_actualizacion = $fecha_actualizacion;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|Tarea[]
     */
    public function getTareas(): Collection
    {
        return $this->tareas;
    }

    // public function addTarea(Tarea $tarea): self
    // {
    //     if (!$this->tareas->contains($tarea)) {
    //         $this->tareas[] = $tarea;
    //         $tarea->setUsuario($this);
    //     }

    //     return $this;
    // }

    // public function removeTarea(Tarea $tarea): self
    // {
    //     if ($this->tareas->removeElement($tarea)) {
    //         // set the owning side to null (unless already changed)
    //         if ($tarea->getUsuario() === $this) {
    //             $tarea->setUsuario(null);
    //         }
    //     }

    //     return $this;
    // }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateFechaActualizacion()
    {
        // update the modified time
        $this->setFechaActualizacion(new \DateTime());
    }
}
