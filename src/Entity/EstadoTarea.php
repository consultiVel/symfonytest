<?php

namespace App\Entity;

use App\Repository\EstadoTareaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EstadoTareaRepository::class)
 */
class EstadoTarea
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_creacion;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_actualizacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity=Tarea::class, mappedBy="Estado")
     */
    private $tareas;

    /**
     * @ORM\OneToMany(targetEntity=TareaHistorial::class, mappedBy="Estado")
     */
    private $tareaHistorials;

    public function __construct()
    {
        $this->tareas = new ArrayCollection();
        $this->tareaHistorials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fecha_creacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fecha_creacion): self
    {
        $this->fecha_creacion = $fecha_creacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fecha_actualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fecha_actualizacion): self
    {
        $this->fecha_actualizacion = $fecha_actualizacion;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|Tarea[]
     */
    public function getTareas(): Collection
    {
        return $this->tareas;
    }

    public function addTarea(Tarea $tarea): self
    {
        if (!$this->tareas->contains($tarea)) {
            $this->tareas[] = $tarea;
            $tarea->setEstado($this);
        }

        return $this;
    }

    public function removeTarea(Tarea $tarea): self
    {
        if ($this->tareas->removeElement($tarea)) {
            // set the owning side to null (unless already changed)
            if ($tarea->getEstado() === $this) {
                $tarea->setEstado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TareaHistorial[]
     */
    public function getTareaHistorials(): Collection
    {
        return $this->tareaHistorials;
    }

    public function addTareaHistorial(TareaHistorial $tareaHistorial): self
    {
        if (!$this->tareaHistorials->contains($tareaHistorial)) {
            $this->tareaHistorials[] = $tareaHistorial;
            $tareaHistorial->setEstado($this);
        }

        return $this;
    }

    public function removeTareaHistorial(TareaHistorial $tareaHistorial): self
    {
        if ($this->tareaHistorials->removeElement($tareaHistorial)) {
            // set the owning side to null (unless already changed)
            if ($tareaHistorial->getEstado() === $this) {
                $tareaHistorial->setEstado(null);
            }
        }

        return $this;
    }
}
