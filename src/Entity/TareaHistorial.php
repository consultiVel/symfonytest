<?php

namespace App\Entity;

use App\Repository\TareaHistorialRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TareaHistorialRepository::class)
 */
class TareaHistorial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $tarea_id;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_creacion;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $fecha_actualizacion;

    /**
     * @ORM\ManyToOne(targetEntity=Tarea::class, inversedBy="tareaHistorials")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Tarea;

    /**
     * @ORM\Column(type="string")
     */
    private $estado_id;

    /**
     * @ORM\ManyToOne(targetEntity=EstadoTarea::class, inversedBy="tareaHistorials")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTareaId(): ?int
    {
        return $this->tarea_id;
    }

    public function setTareaId(int $tarea_id): self
    {
        $this->tarea_id = $tarea_id;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fecha_creacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fecha_creacion): self
    {
        $this->fecha_creacion = $fecha_creacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fecha_actualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fecha_actualizacion): self
    {
        $this->fecha_actualizacion = $fecha_actualizacion;

        return $this;
    }

    public function getTarea(): ?Tarea
    {
        return $this->Tarea;
    }

    public function setTarea(?Tarea $Tarea): self
    {
        $this->Tarea = $Tarea;

        return $this;
    }

    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    public function setEstadoId(int $estado_id): self
    {
        $this->estado_id = $estado_id;

        return $this;
    }

    public function getEstado(): ?EstadoTarea
    {
        return $this->Estado;
    }

    public function setEstado(?EstadoTarea $Estado): self
    {
        $this->Estado = $Estado;

        return $this;
    }

    public function __construct()
    {
        // we set up "created"+"modified"
        $this->setFechaCreacion(new \DateTime());
        if ($this->getFechaActualizacion() == null) {
            $this->setFechaActualizacion(new \DateTime());
        }
       
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateFechaActualizacion()
    {
        // update the modified time
        $this->setFechaActualizacion(new \DateTime());
    }
}
