<?php

namespace App\Entity;

use App\Repository\TareaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TareaRepository::class)
 */
class Tarea
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $usuario_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Usuario::class, inversedBy="tareas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Usuario;

    /**
     * @ORM\ManyToOne(targetEntity=EstadoTarea::class, inversedBy="tareas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Estado;

    /**
     * @ORM\OneToMany(targetEntity=TareaHistorial::class, mappedBy="Tarea")
     */
    private $tareaHistorials;

    public function __construct()
    {
        $this->tareaHistorials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuarioId(): ?int
    {
        return $this->usuario_id;
    }

    public function setUsuarioId(int $usuario_id): self
    {
        $this->usuario_id = $usuario_id;

        return $this;
    }

    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    public function setEstadoId(int $estado_id): self
    {
        $this->estado_id = $estado_id;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->Usuario;
    }

    public function setUsuario(?Usuario $Usuario): self
    {
        $this->Usuario = $Usuario;

        return $this;
    }

    public function getEstado(): ?EstadoTarea
    {
        return $this->Estado;
    }

    public function setEstado(?EstadoTarea $Estado): self
    {
        $this->Estado = $Estado;

        return $this;
    }

    /**
     * @return Collection|TareaHistorial[]
     */
    public function getTareaHistorials(): Collection
    {
        return $this->tareaHistorials;
    }

    public function addTareaHistorial(TareaHistorial $tareaHistorial): self
    {
        if (!$this->tareaHistorials->contains($tareaHistorial)) {
            $this->tareaHistorials[] = $tareaHistorial;
            $tareaHistorial->setTarea($this);
        }

        return $this;
    }

    public function removeTareaHistorial(TareaHistorial $tareaHistorial): self
    {
        if ($this->tareaHistorials->removeElement($tareaHistorial)) {
            // set the owning side to null (unless already changed)
            if ($tareaHistorial->getTarea() === $this) {
                $tareaHistorial->setTarea(null);
            }
        }

        return $this;
    }
}
