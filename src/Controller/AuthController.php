<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use App\Form\UsuarioLoginType;
use App\Form\UsuarioType;


class AuthController extends AbstractApiController
{
    /**
     * @Route("/api/register", name="register",  methods={"POST"})
     */
    public function register(Request $request)
    {
        $usuario = new Usuario();

        $form = $this->buildForm(UsuarioType::class);

        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Usuario $usuario */
            $usuario = $form->getData();

            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();

            return $this->respond(['data' => $usuario]);
        }


        return $this->respond($form, Response::HTTP_BAD_REQUEST);
    }


    /**
     * @Route("/api/login", name="login",  methods={"POST"})
     */
    public function login(Request $request, UsuarioRepository $usuarioRepository)
    {
        $form = $this->buildForm(UsuarioLoginType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $clave = $request->request->get('clave');
            $correo = $request->request->get('correo');

            $user = $usuarioRepository->getLogin(['clave' => $clave, 'correo' => $correo]);
           
            if ($user) return $this->respond(['data' => $user]);

            return $this->respond(['message' => 'Error de credenciales :c'], Response::HTTP_BAD_REQUEST);
        }

        return $this->respond($form, Response::HTTP_BAD_REQUEST);
    }
}
