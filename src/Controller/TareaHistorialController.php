<?php

namespace App\Controller;

use App\Repository\TareaHistorialRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api")
 */
class TareaHistorialController extends AbstractApiController
{
    /**
     * @Route("/tarea/{tareaId}/historial", name="tarea_historial", methods={"GET"})
     */
    public function index($tareaId = null, TareaHistorialRepository $historialRepo): Response
    {
        
        return $this->json(['data' => $historialRepo->findByTareaTask($tareaId)], 200);
    }


    /**
     * @Route("/tarea/{tareaId}/historial", name="save_tarea_historial", methods={"POST"})
     */
    public function save(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TareaHistorialController.php',
        ]);
    }
}
