<?php

namespace App\Controller;

use App\Entity\EstadoTarea;
use App\Entity\Tarea;
use App\Entity\TareaHistorial;
use App\Form\TareaType;
use App\Repository\TareaRepository;
use App\Repository\EstadoTareaRepository;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TareaController extends AbstractApiController
{



    function __construct(
        UsuarioRepository $usuarioRepository,
        EstadoTareaRepository $estadoTareaRepository,
        TareaRepository $tareaRepository
    ) {
        $this->usuarioRepository = $usuarioRepository;
        $this->estadoTareaRepository = $estadoTareaRepository;
        $this->tareaRepository = $tareaRepository;
    }

    /**
     * @Route("/tareas", name="tarea_index", methods={"GET"})
     */
    public function index(Request $request, TareaRepository $tareaRepository): Response
    {
        $tareas =  $tareaRepository->findByUserTask($request->query->get('id'));
        return $this->json(['data' => $tareas], 200);
    }

    /**
     * @Route("/tareas", name="tarea_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $tarea = new Tarea();
        $historial = new TareaHistorial();
        $estado = $this->estadoTareaRepository->find($request->request->get('estado_id'));
        $usuario = $this->usuarioRepository->find($request->request->get('usuario_id'));



        $form = $this->buildForm(TareaType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Tarea $tarea */
            $tarea = $form->getData();
            $tarea->setUsuario($usuario);
            $tarea->setEstado($estado);


            $historial->setEstado($estado);
            $historial->setTarea($tarea);


            $this->getDoctrine()->getManager()->persist($tarea);
            $this->getDoctrine()->getManager()->persist($historial);
            $this->getDoctrine()->getManager()->flush();

            //get Tarea 
            $response = $this->tareaRepository->getOneTask($tarea->getId());
            return $this->respond(['data' => $response]);
           

        }

        return $this->respond($form, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/tareas/{id}", name="tarea_show", methods={"GET"})
     */
    public function show($id): Response
    {

         //get Tarea 
           
         $tarea = $this->tareaRepository->getOneTask($id);
        if ($tarea) return $this->respond(['data' => $tarea]);
        return $this->respond(['message' => 'Tarea inexistente'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/tareas/{id}/edit", name="tarea_edit", methods={"PUT"})
     */
    public function edit(Request $request,  $id): Response
    {
        $tarea = $this->tareaRepository->find($id);

        if (!$tarea) return $this->respond(['message' => 'Tarea inexistente'], Response::HTTP_BAD_REQUEST);


        $form = $this->buildForm(TareaType::class);
        $form->submit($request);


        if ($form->isSubmitted()) {
           
            /** @var Tarea $tarea */
            $tarea->setTitulo($request->request->get('titulo'));
            $tarea->setDescription($request->request->get('description'));
            $tarea->setUsuarioId($request->request->get('usuario_id'));
            $tarea->setEstadoId($request->request->get('estado_id'));

            $this->getDoctrine()->getManager()->flush();


            //get Tarea 
            $response = $this->tareaRepository->getOneTask($tarea->getId());
            return $this->respond(['data' => $response]);
        }
    }

    /**
     * @Route("/tareas/{id}", name="tarea_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tarea $tarea): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tarea);
        $entityManager->flush();

        return $this->respond(['data' => "tarea eliminada"]);
    }
}
