<?php

namespace App\Form;

use App\Entity\Tarea;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\NotNull;

class TareaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuario_id', IntegerType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Usuario requerido'])
                ]
            ])
            ->add('estado_id', IntegerType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Estado requerido'])
                ]
            ])
            ->add('titulo', TextType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Titulo requerida'])
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Descripcion requerida'])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tarea::class,
            'csrf_protection' => false
        ]);
    }
}
