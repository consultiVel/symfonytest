<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;


class UsuarioLoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('correo', EmailType::class, [
                'constraints' => [
                    new NotNull(['message' =>'Correo requerido']),
                    new Email(['message' =>'Correo inválido'])
                ]
            ])
            ->add('clave', TextType::class, [
                'constraints' => [
                    new NotNull(['message' =>'Clave requerida'])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
            'csrf_protection' => false
        ]);
    }
}
