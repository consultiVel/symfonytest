<?php

namespace App\Repository;

use App\Entity\TareaHistorial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TareaHistorial|null find($id, $lockMode = null, $lockVersion = null)
 * @method TareaHistorial|null findOneBy(array $criteria, array $orderBy = null)
 * @method TareaHistorial[]    findAll()
 * @method TareaHistorial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TareaHistorialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TareaHistorial::class);
    }

    /**
    * @return TareaHistorial[] Returns an array of TareaHistorial objects
    */
    
    public function findByTareaTask($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.tarea_id = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?TareaHistorial
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
