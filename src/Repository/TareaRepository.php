<?php

namespace App\Repository;

use App\Entity\Tarea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tarea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarea[]    findAll()
 * @method Tarea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TareaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tarea::class);
    }

    // /**
    //  * @return Tarea[] Returns an array of Tarea objects
    //  */

    public function findByUserTask($id)
    {
        $con = $this->getEntityManager()->getConnection();

        $sql = "
        SELECT tarea.*,estado_tarea.nombre as nombre_estado FROM tarea INNER JOIN estado_tarea on tarea.estado_id = estado_tarea.id
        WHERE tarea.usuario_id = ${id}
        ORDER BY tarea.id ASC;        
        
        ";

        $stmt = $con->prepare($sql);

        $stmt->execute();


        return $stmt->fetchAllAssociative();;
    }


    public function getOneTask($id)
    {
        $con = $this->getEntityManager()->getConnection();

        $sql = "

        SELECT tarea.*,estado_tarea.nombre as nombre_estado FROM tarea INNER JOIN estado_tarea on tarea.estado_id = estado_tarea.id
        WHERE tarea.id = ${id} ;        
        
        ";
        $stmt = $con->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAssociative();
    }


  
}
