<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721145743 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarea_historial ADD estado_id INT NOT NULL');
        $this->addSql('ALTER TABLE tarea_historial ADD CONSTRAINT FK_39A186619F5A440B FOREIGN KEY (estado_id) REFERENCES estado_tarea (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_39A186619F5A440B ON tarea_historial (estado_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tarea_historial DROP CONSTRAINT FK_39A186619F5A440B');
        $this->addSql('DROP INDEX IDX_39A186619F5A440B');
        $this->addSql('ALTER TABLE tarea_historial DROP estado_id');
    }
}
