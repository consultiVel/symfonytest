<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720231817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE estado_tarea_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tarea_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE usuario_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE estado_tarea (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_creacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, fecha_actualizacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, estado INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE tarea (id INT NOT NULL, usuario_id INT NULL, estado_id INT NULL, titulo VARCHAR(255) NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3CA05366DB38439E ON tarea (usuario_id)');
        $this->addSql('CREATE INDEX IDX_3CA053669F5A440B ON tarea (estado_id)');
        $this->addSql('CREATE TABLE usuario (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, correo VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, clave VARCHAR(255) NOT NULL, fecha_creacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, fecha_actualizacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, estado INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE tarea ADD CONSTRAINT FK_3CA05366DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tarea ADD CONSTRAINT FK_3CA053669F5A440B FOREIGN KEY (estado_id) REFERENCES estado_tarea (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tarea DROP CONSTRAINT FK_3CA053669F5A440B');
        $this->addSql('ALTER TABLE tarea DROP CONSTRAINT FK_3CA05366DB38439E');
        $this->addSql('DROP SEQUENCE estado_tarea_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tarea_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE usuario_id_seq CASCADE');
        $this->addSql('DROP TABLE estado_tarea');
        $this->addSql('DROP TABLE tarea');
        $this->addSql('DROP TABLE usuario');
    }
}
