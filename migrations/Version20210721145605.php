<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721145605 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE tarea_historial_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE tarea_historial (id INT NOT NULL, tarea_id INT NOT NULL, fecha_creacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, fecha_actualizacion TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_39A186616D5BDFE1 ON tarea_historial (tarea_id)');
        $this->addSql('ALTER TABLE tarea_historial ADD CONSTRAINT FK_39A186616D5BDFE1 FOREIGN KEY (tarea_id) REFERENCES tarea (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE tarea_historial_id_seq CASCADE');
        $this->addSql('DROP TABLE tarea_historial');
    }
}
